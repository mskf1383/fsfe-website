<?xml version="1.0" encoding="UTF-8"?>

<data>
  <version>2</version>
  <module>

    <h2>I ♥ Free Software Day 2022</h2>

    <p>
      This year's I Love Free Software Day is all about Free Software games. 
      Games, and especially Free Software games, are not only a fun activity but
      also are an important part of our daily lives. While playing we can meet new and old 
      friends, can relax and learn new skills, and most important we can contribute
      to our favourite Free Software game and adapt it to our own 
      wishes. Therefore the FSFE are planning a special event dedicated to 
      Free Software games. We will have three experts in Free Software 
      games, games engines and Game Jams.</p>
      
      <p> The <a href="https://download.fsfe.org/campaigns/ilovefs/games/Games%20Event/IloveFS22%20Agenda.pdf">agenda of the event</a> looks like this:
      <ul>
      <li>18:00 - 18:05 (CET): Welcome and Introduction</li>
      <li>18:05 - 18:25 (CET): Flare - Free/Libre Action Roleplaying Engine by Justin Jacobs</li>
      <li>18:25 - 18:45 (CET): Vassal – Free Software Game Engine by Joel Uckelman</li>
      <li>18:45 - 19:05 (CET): Godot Wild Jams by Kati Baker</li>
      <li>19:05 - 19:20 (CET): Veloren –Free Software Game by Forest Anderson</li>
      <li>19:20 - 20:00 (CET): Gaming time</li>
      <li>20:00 (CET): Closing remarks</li>
      </ul>
      
      For more information on the 
      event have a look at our <a 
      href="/news/2022/news-20220201-01.html">news 
      page</a>.
    </p>

    <p>
      But we also say <strong>thank you</strong> to all of you who have 
      contributed to Free Software projects. Let us all use the I Love Free 
      Software day to say thank you and celebrate our love for Free 
      Software.
    </p>

    <figure>
      <img
        src="https://pics.fsfe.org/uploads/medium/69395a3acc8cfb98fbb058d0b44a8f04.png"
        alt="Gaming event picture" />
          <figcaption>
          Let's celebrate #ilovefs day!
          </figcaption>
    </figure>

    <p>
      For this year's I Love Free Software Day we have a
      new Software Freedom Podcast episode. Our guest for this special 
      <a href="/news/podcast/episode-13.html">Software Freedom Podcast episode</a> is Stanislas Dolcini; together 
      Bonnie and Stanislas talk about the Free Software game 0.A.D: Empires Ascendant.
    </p>


    <h2>Be part of the I Love Free Software Day</h2>

    
      <div class="icon-grid">
      <ul>
        <li>
          <img src="/graphics/icons/ilovefs-use.png" 
          alt="ILoveFS heart with 'use'"/>
          <div>
            <h3>Use the opportunity</h3>
            <p>
              Take the opportunity this wonderful day to thank the people who
              enable you to enjoy software freedom. Make use of our 
              new <a 
              href="https://download.fsfe.org/campaigns/ilovefs/games/">I 
              Love Free Software: Game event graphics</a> and our 
              special <a href="/news/podcast/episode-13.html">Software Freedom Podcast episode</a>,
              <a href="https://download.fsfe.org/campaigns/ilovefs/share-pics/">sharepics</a>,
              the <a href="/contribute/spreadtheword#ilovefs">stickers and
              balloons</a>, <a href="/activities/ilovefs/artwork/artwork.html">artwork</a>, and
              <a href="/order/order.html">merchandise</a> the FSFE provides for
              <em>#ilovefs</em>.
            </p>

          </div>
        </li>

        <li>
          <img 
          src="/graphics/icons/ilovefs-study.png" 
          alt="ILoveFS heart with 'study'"/>
          <div>
            <h3>Think about it</h3>
            <p>
              Think about which Free Software project you have used throughout the 
              last year. Which Free Software game have you played with your friends
              or on your own? How did you have fun while or because of using Free Software? 
              For some inspiration have a look at others' <a 
              href="/activities/ilovefs/whylovefs/gallery.html">love 
              statements</a>.
            </p>
          </div>
        </li>

        <li>
          <img 
          src="/graphics/icons/ilovefs-share.png" 
          alt="ILoveFS heart with 'share'"/>
          <div>
            <h3>Share your love</h3>
            <p>
              That's the fun part! We have created a <a 
              href="https://sharepic.fsfe.org/">sharepic generator</a>, 
              with which you can easily create your very own sharepics 
              and share your appreciation on social media
              (<em>#ilovefs</em>), in blog posts, pictures and video messages,
              or directly to the Free Software developers and contributors.
            </p>
          </div>
        </li>

        <li>
          <img 
          src="/graphics/icons/ilovefs-improve.png" 
          alt="ILoveFS heart with 'improve'"/>
          <div>
            <h3>Improve Free Software</h3>
            <p>
              Talking is silver, contributing is gold! Help a Free Software
              project with code, a translation, or by assisting its users. Or if
              you can, please consider a donation to a Free Software
              organisation <a href="https://my.fsfe.org/donate">like the
              FSFE</a>, or to a Free Software project.
            </p>
          </div>
        </li>
      </ul>
    </div>

    <figure class="no-border">
      <img
        src="https://pics.fsfe.org/uploads/small/f164c8517e78449fc8844a72347af490.png"
        alt="#ilovefs" />
    </figure>

    <p>
      What is your contribution to this special day and the people who enable
      you to use Free Software? Will you use our stickers and balloons? Or make
      a picture or video with your new ILoveFS shirt? And what about celebrating
      software freedom with your colleagues and friends at a company gathering
      or public event? Whatever you do, show it to the world by using the
      <em>#ilovefs</em> tag. And if you have questions, <a href="/contact/">just
      drop us an email</a>.
  </p>

    <p>
      Happy <strong><span class="text-danger">I Love Free
      Software</span></strong> Day everyone! ❤
    </p>

  </module>
</data>

